module API.Errors (
  APIError(..),
  ViewErrors(..)
) where

import Data.Aeson
import Data.Aeson.Types
import Data.Text
import Data.UUID

data APIError
  = Unknown Text
  | AuthenticationDown
  | Unauthorized
  | BadCredentials
  | UserExists
  | DBError Text
  | BadRequest Text
  | NotFound UUID

newtype ViewErrors = ViewErrors [APIError]

(.=.) :: Text -> Text -> Pair
k .=. v = k .= v

instance ToJSON APIError where
  toJSON (Unknown t) =
      object [ "type" .=. "unknown"
             , "description" .= t
             ]
  toJSON AuthenticationDown =
    object [ "type" .=. "authentication_down" ]
  toJSON Unauthorized =
    object [ "type" .=. "unauthorized" ]
  toJSON BadCredentials =
    object [ "type" .=. "incorrect_credentials" ]
  toJSON (DBError t) =
    object [ "type" .=. "database_error"
           , "description" .= t
           ]
  toJSON UserExists =
    object [ "type" .=. "user_exists"
           , "description" .=. "This email is already registered."
           ]
  toJSON (BadRequest desc) =
    object [ "type" .=. "bad_request"
           , "description" .= desc
           ]
  toJSON (NotFound id') =
    object [ "type" .=. "not_found"
           , "id" .= toText id'
           , "description" .=. "Entity not found"
           ]

instance ToJSON ViewErrors where
  toJSON (ViewErrors es) =
    object [ "errors" .= toJSON es ]
