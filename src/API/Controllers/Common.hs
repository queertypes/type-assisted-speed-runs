module API.Controllers.Common (
  getSession,
  setSession,
  deleteSession,

  ok,
  created,
  noContent,
  redirect,
  notModified,

  errorResponse
) where

import Control.Monad.IO.Class
import Data.Aeson (ToJSON)
import Data.UUID (UUID, toText)
import Data.Text (Text)
import Web.Spock.Shared
import qualified Network.HTTP.Types.Status as HTTP

import API.Errors

sessionKey :: Text
sessionKey = "SessionKey"

setSession :: MonadIO m => UUID -> ActionT m ()
setSession skey =
  setCookie sessionKey (toText skey) 120000

getSession :: MonadIO m => ActionT m (Maybe Text)
getSession = cookie sessionKey

deleteSession :: MonadIO m => ActionT m ()
deleteSession = deleteCookie sessionKey

ok :: (ToJSON a, MonadIO m) => a -> ActionT m b
ok a = setStatus HTTP.status200 >> json a

created :: (ToJSON a, MonadIO m) => a -> ActionT m b
created a = setStatus HTTP.status201 >> json a

noContent :: MonadIO m => ActionT m ()
noContent = setStatus HTTP.status204

notModified :: MonadIO m => ActionT m ()
notModified = setStatus HTTP.status304

badRequest :: (ToJSON a, MonadIO m) => a -> ActionT m b
badRequest e = setStatus HTTP.status400 >> json e

unauthorized :: (ToJSON a, MonadIO m) => a -> ActionT m b
unauthorized e = setStatus HTTP.status401 >> json e

forbidden :: (ToJSON a, MonadIO m) => a -> ActionT m b
forbidden e = setStatus HTTP.status403 >> json e

notFound :: (ToJSON a, MonadIO m) => a -> ActionT m b
notFound e = setStatus HTTP.status404 >> json e

{-
rateLimited :: (ToJSON a, MonadIO m) => a -> ActionT m b
rateLimited e = setStatus HTTP.status429 >> json e

serverError :: (ToJSON a, MonadIO m) => a -> ActionT m b
serverError e = setStatus HTTP.status500 >> json e
-}

unavailable :: (ToJSON a, MonadIO m) => a -> ActionT m b
unavailable e = setStatus HTTP.status503 >> json e

errorResponse :: (MonadIO m) => APIError -> ActionT m a
errorResponse e@(Unknown _) = badRequest e
errorResponse e@AuthenticationDown = unavailable e
errorResponse e@Unauthorized = unauthorized e
errorResponse e@BadCredentials = forbidden e
errorResponse e@UserExists = badRequest e
errorResponse e@(DBError _) = badRequest e
errorResponse e@(BadRequest _) = badRequest e
errorResponse e@(NotFound _) = notFound e
