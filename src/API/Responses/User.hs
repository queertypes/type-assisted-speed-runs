module API.Responses.User (
  ViewUser(..)
) where

import Data.Aeson

import Data.UUID
import API.Models.Common
import API.Models.User

newtype ViewUser = ViewUser User

instance ToJSON ViewUser where
  toJSON (ViewUser u) =
    let (Email email') = email u
        (PlayerId id') = pid u
        (DisplayName name') = name u
        (Joined joined') = joined u
        (LastLogin login') = lastLogin u
    in object [ "id" .= toText id'
              , "email" .= email'
              , "display_name" .= name'
              , "joined" .= toJSON joined'
              , "last_login" .= toJSON login'
              ]
