{-# LANGUAGE OverloadedStrings, ScopedTypeVariables #-}
module API.Routes where

import Web.Spock.Safe
import qualified Hasql as H
import qualified Hasql.Postgres as HP
import qualified Database.Redis as R

import API.Controllers.Login
import API.Controllers.Players
import API.Controllers.Register
import API.Database.Common
import API.Logging (mkLog, Log)

--------------------------------------------------------------------------------
main :: IO ()
main = do
  let psqlConf = HP.ParamSettings "localhost" 5432 "strangeloop" "cat" "strangeloop"
  let poolConf = maybe (error "Bad pool settings") id $ H.poolSettings 6 30
  sqlC :: Pool
    <- H.acquirePool psqlConf poolConf
  redisC <- R.connect R.defaultConnectInfo
  logger <- mkLog
  runSpock 3000 $ spockT id $
    do core sqlC redisC logger
       players sqlC
       games
       runs

core :: Pool -> R.Connection -> Log -> SpockT IO ()
core sqlC redisC logger =
  do post "register" (registerUser sqlC logger)
     post "login" (login sqlC redisC logger)

players :: Pool -> SpockT IO ()
players sqlC =
  do get ("players" <//> var) (\playerId -> viewPlayer sqlC playerId)
     get "players" $ text ""
     put ("players" <//> var <//> "profile") $ \x -> text x
     delete ("players" <//> var) $ \x -> text x

games :: SpockT IO ()
games =
  do get "games" $ text ""
     get ("games" <//> var) $ \x -> text x

runs :: SpockT IO ()
runs =
  do get "runs" (text "")
     post ("games" <//> var <//> "runs") $ \x -> text x
     get ("runs" <//> var) $ \x -> text x

--------------------------------------------------------------------------------
