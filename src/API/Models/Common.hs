module API.Models.Common (
  PlayerId(..),
  RunId(..),
  GameId(..),
  DisplayName(..),
  Joined(..),
  LastLogin(..),
  Email(..),
  Pass(..),
  PassHash(..),

  hashPass
) where

import Data.UUID
import Data.ByteString
import Data.Text
import Data.Time.Clock
import Data.Text.Encoding (encodeUtf8)

newtype PlayerId = PlayerId UUID
newtype RunId = RunId UUID
newtype GameId = GameId UUID
newtype DisplayName = DisplayName Text
newtype Joined = Joined UTCTime
newtype LastLogin = LastLogin UTCTime
newtype Email = Email Text
newtype Pass = Pass Text
newtype PassHash = PassHash ByteString

hashPass :: Pass -> PassHash
hashPass (Pass t) = PassHash (encodeUtf8 t)
