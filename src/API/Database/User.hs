{-# LANGUAGE FlexibleContexts, QuasiQuotes, RankNTypes, ConstraintKinds #-}
module API.Database.User (
  authenticate,
  register,
  findUserByEmail,
  createUser,
  lookupPlayerById,
  AuthOutcome(..)
) where

import Data.UUID

import Data.ByteString (ByteString)
import Data.Monoid
import Data.Text (Text, pack)
import Data.Time.Clock
import System.Entropy
import qualified Hasql as H
import qualified Data.ByteString.Base16 as B16
import qualified Data.Text.Encoding as T

import API.Database.Common
import API.Errors
import API.Models.Common
import API.Models.User hiding (email)

data AuthOutcome
  = AuthSuccess
  | AuthFailed

-- | Authenticates a user.
authenticate :: (Cx, Val Text, Val ByteString) =>
  Pool -> Email -> Pass -> IO (Either APIError AuthOutcome)
authenticate sqlC (Email email) (Pass pass) = do
  let pass_hash = T.encodeUtf8 pass

  -- transaction: lookup a user in the DB with these credentials
  ret <- H.session sqlC $ H.tx serialR $ do
    (v :: Maybe (One UUID)) <- H.maybeEx $ [H.stmt|
      SELECT t.id
      FROM tas.player t
      WHERE t.email = $email
        AND t.password_hash = $pass_hash
    |]
    return v

  return $ case ret of
    (Left e) -> Left (DBError (pack . show $ e))
    (Right Nothing) -> Right AuthFailed
    (Right (Just _)) -> Right AuthSuccess

-- | Register a user, if they don't already exist
register :: Pool -> Email -> Pass -> IO (Either APIError User)
register sqlC email (Pass pass) = do
  let pass_hash = T.encodeUtf8 pass
  now <- getCurrentTime
  rand <- getEntropy 8
  let randAppend = T.decodeUtf8 . B16.encode $ rand
  let displayName = "runner" <> randAppend

  -- transaction: register a user if the email isn't in the DB
  x <- H.session sqlC $ H.tx serialW $ do
    (exists :: Maybe (One UUID)) <- findUserByEmail email
    case exists of
      Just _ -> return Nothing
      Nothing -> Just <$> createUser email pass_hash displayName now

  return $ case x of
    (Left b) -> Left (DBError (pack . show $ b))
    (Right Nothing) -> Left UserExists
    (Right (Just uuid)) ->
      Right $ User (PlayerId $ one uuid) (DisplayName displayName) email
              (PassHash pass_hash) (Joined now) (LastLogin now)


type DBPlayer = (Text, Text, ByteString, UTCTime, UTCTime)

lookupPlayerById :: Pool -> PlayerId -> IO (Either APIError User)
lookupPlayerById sqlC (PlayerId pid') = do
  x <- H.session sqlC $ H.tx serialR $ do
    (exists :: Maybe DBPlayer) <- H.maybeEx $ [H.stmt|
      SELECT t.display_name, t.email, t.password_hash, t.joined, t.last_login
      FROM tas.player t
      WHERE t.id = $pid'
    |]
    return exists

  return $ case x of
    (Left e) -> Left (DBError (pack . show $ e))
    (Right Nothing) -> Left (NotFound pid')
    (Right (Just (dname, email', phash', joined', lastLogin'))) ->
      Right $ User (PlayerId pid') (DisplayName dname) (Email email')
              (PassHash phash') (Joined joined') (LastLogin lastLogin')

findUserByEmail :: Email -> Tx (Maybe (One UUID))
findUserByEmail (Email email) =
  H.maybeEx $ [H.stmt|
      SELECT tas.player.id
      FROM tas.player
      WHERE tas.player.email = $email
    |]

createUser :: Email -> ByteString -> Text -> UTCTime
           -> Tx (One UUID)
createUser (Email email) pass_hash dname now =
  H.singleEx $ [H.stmt|
    INSERT INTO tas.player (display_name, email,
                            password_hash, joined, last_login)
    VALUES ($dname, $email,
            $pass_hash, $now, $now)
    RETURNING tas.player.id
    |]
